package com.example.recyclerviewtest1;

import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private ArrayList<PhoneNumber> phoneNumberArrayList;
    private PhoneAdapter phoneAdapter;
    private Context context;
    private Button button;
    private Button submitBtn;
    private TextView submitText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mRecyclerView = findViewById(R.id.recyclerview);
        button = findViewById(R.id.second_activity);
        submitBtn = findViewById(R.id.submit_number);
        submitText = findViewById(R.id.submit_text);




        context = this;

        if (phoneNumberArrayList == null){
            phoneNumberArrayList = new ArrayList<>();
        }

        PhoneNumber phoneNumber = new PhoneNumber();
        phoneNumber.setPhone("1822902985");
        phoneNumber.setProperty("(账号号码)");
        phoneNumberArrayList.add(phoneNumber);

        PhoneNumber phoneNumber1 = new PhoneNumber();
        phoneNumber1.setPhone("13060817517");
        phoneNumber1.setProperty("(收件人号码)");
        phoneNumberArrayList.add(phoneNumber1);

        PhoneNumber phoneNumber2 = new PhoneNumber();
        phoneNumber2.setPhone("15875196982");
        phoneNumber2.setProperty("(收件人号码)");
        phoneNumberArrayList.add(phoneNumber2);

        PhoneNumber phoneNumber3 = new PhoneNumber();
        phoneNumber3.setPhone("15819579762");
        phoneNumber3.setProperty("(收件人号码)");
        phoneNumberArrayList.add(phoneNumber3);

        PhoneNumber phoneNumber4 = new PhoneNumber();
        phoneNumber4.setPhone("15875198519");
        phoneNumber4.setProperty("(收件人号码)");
        phoneNumberArrayList.add(phoneNumber4);

        LinearLayoutManager manager = new LinearLayoutManager(context);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        //RecyclerView.LayoutManager manager = new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false);
        //GridLayoutManager manager1 = new GridLayoutManager(context,2);
        //manager1.setOrientation(GridLayoutManager.VERTICAL);
        //StaggeredGridLayoutManager manager2 = new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(manager);


        phoneAdapter = new PhoneAdapter(context, phoneNumberArrayList);
        mRecyclerView.setAdapter(phoneAdapter);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                startActivity(intent);
            }
        });

        submitBtn.setOnClickListener(view -> {
            PhoneNumber phoneNumber5 = phoneAdapter.getPhoneNumber();
            submitText.setText(phoneNumber5.getPhone());
        });

    }


    class PhoneAdapter extends RecyclerView.Adapter<PhoneAdapter.PhoneViewHolder>{

        private Context mContext;
        private ArrayList<PhoneNumber> phoneNumbers;
        private View view;

        private boolean isClick;
        private int currentPosition;
        private boolean isNerverClick = true; //从来没被点击过

        public int getCurrentPosition() {
            if (isNerverClick){
                return 0;
            }
            return currentPosition;
        }

        public boolean isClick() {
            return isClick;
        }

        public void setCurrentPosition(int currentPosition , boolean isClick) {
            this.currentPosition = currentPosition;
            this.isClick = isClick;
        }

        public PhoneAdapter(Context mContext, ArrayList<PhoneNumber> phoneNumbers) {
            this.mContext = mContext;
            this.phoneNumbers = phoneNumbers;
        }

        @NonNull
        @Override
        public PhoneViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            view = LayoutInflater.from(mContext).inflate(R.layout.recyclerview_item, parent ,false);
            PhoneViewHolder holder = new PhoneViewHolder(view);
            ItemClickEvent(holder);
            return holder;
        }

        public PhoneNumber getPhoneNumber(){
            return phoneNumbers.get(getCurrentPosition());
        }

        private void ItemClickEvent(PhoneViewHolder holder) {
            holder.itemView.setOnClickListener(view -> {
                isNerverClick = false;
                if (!isClick){
                    setCurrentPosition(holder.getBindingAdapterPosition(), true);
                }else {
                    setCurrentPosition(holder.getBindingAdapterPosition(), getCurrentPosition() != holder.getBindingAdapterPosition());
                }
                Log.e("MAINACTIVITY","打印出选中的选项"+phoneNumbers.get(getCurrentPosition()).getPhone()+"");
                notifyDataSetChanged();
             });
        }

        @Override
        public void onBindViewHolder(@NonNull PhoneViewHolder holder, int position) {
            PhoneNumber phoneNumber = phoneNumbers.get(position);
            holder.phoneNumber.setText(phoneNumber.getPhone());
            holder.phoneProperty.setText(phoneNumber.getProperty());
            if (isNerverClick){
                if (position == 0){
                    holder.imageView.setImageResource(R.drawable.select);
                }
            } else {
                if (getCurrentPosition() == position && isClick()){
                    holder.imageView.setImageResource(R.drawable.select);
                }else {
                    holder.imageView.setImageResource(R.drawable.unselect);
                }
            }
        }

        @Override
        public int getItemCount() {
            return phoneNumbers != null ?phoneNumbers.size() : 0;
        }

        class PhoneViewHolder extends RecyclerView.ViewHolder{

            ImageView imageView;
            TextView phoneNumber;
            TextView phoneProperty;
            RelativeLayout callBckLayout;

            public PhoneViewHolder(@NonNull View itemView) {
                super(itemView);
//                callBckLayout = (RelativeLayout) itemView;
                imageView = itemView.findViewById(R.id.recycler_image);
                phoneNumber = itemView.findViewById(R.id.recyclerview_phone_number);
                phoneProperty = itemView.findViewById(R.id.recyclerview_phone_property);
//                callBckLayout.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        if (!isClick){
//                            setCurrentPosition(getBindingAdapterPosition(), true);
//                        }else {
//                            setCurrentPosition(getBindingAdapterPosition(), getCurrentPosition() != getBindingAdapterPosition());
//                        }
//                        notifyDataSetChanged();
//                    }
//                });
            }
        }
    }

}