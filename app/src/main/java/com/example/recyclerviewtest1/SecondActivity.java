package com.example.recyclerviewtest1;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class SecondActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private Context context;
    private AddGuanXiAdapter addGuanXiAdapter;
    private List<FamilyBean> familyBeans;
    private Button secondBtn;
    private TextView secondText;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        setContentView(R.layout.activity_second);

        mRecyclerView = findViewById(R.id.second_recyclerview);

        secondBtn = findViewById(R.id.submit_second_number);
        secondText = findViewById(R.id.submit_second_text);

        if (familyBeans == null){
            familyBeans = new ArrayList<>();
        }

        FamilyBean phoneNumber = new FamilyBean();
        phoneNumber.setName("18822902985");
        phoneNumber.setNum("(账号号码)");
        familyBeans.add(phoneNumber);

        FamilyBean phoneNumber1 = new FamilyBean();
        phoneNumber1.setName("13060817517");
        phoneNumber1.setNum("(收件人号码)");
        familyBeans.add(phoneNumber1);

        FamilyBean phoneNumber2 = new FamilyBean();
        phoneNumber2.setName("15875196982");
        phoneNumber2.setNum("(收件人号码)");
        familyBeans.add(phoneNumber2);

        FamilyBean phoneNumber3 = new FamilyBean();
        phoneNumber3.setName("15819579762");
        phoneNumber3.setNum("(收件人号码)");
        familyBeans.add(phoneNumber3);

        FamilyBean phoneNumber4 = new FamilyBean();
        phoneNumber4.setName("15875198519");
        phoneNumber4.setNum("(收件人号码)");
        familyBeans.add(phoneNumber4);

        LinearLayoutManager manager = new LinearLayoutManager(context);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        //RecyclerView.LayoutManager manager = new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false);
        //GridLayoutManager manager1 = new GridLayoutManager(context,2);
        //manager1.setOrientation(GridLayoutManager.VERTICAL);
        //StaggeredGridLayoutManager manager2 = new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(manager);


        addGuanXiAdapter = new AddGuanXiAdapter(context, familyBeans);

        addGuanXiAdapter.setOnItemListener(new AddGuanXiAdapter.OnItemListener() {
            @Override
            public void onClick(View v, int pos, String projectc) {
                addGuanXiAdapter.setDefSelect(pos);
                String relationship = familyBeans.get(pos).getName();
                Log.e("MAINACTIVITY","打印出来点击到的选项" + relationship);
            }
        });

        mRecyclerView.setAdapter(addGuanXiAdapter);

        secondBtn.setOnClickListener(view -> {
            secondText.setText(addGuanXiAdapter.getFamilyBean().getName());
        });

    }

}