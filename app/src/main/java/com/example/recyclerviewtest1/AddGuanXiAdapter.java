package com.example.recyclerviewtest1;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class AddGuanXiAdapter extends RecyclerView.Adapter<AddGuanXiAdapter.MyViewHolder> {
    private Context context;
    private List<FamilyBean> list;
    private int defItem = -1;//默认值
    private OnItemListener onItemListener;

    public AddGuanXiAdapter(Context context, List<FamilyBean> list) {
        this.context = context;
        this.list = list;
    }

    public void setOnItemListener(OnItemListener onItemListener) {
        this.onItemListener = onItemListener;
    }

    public interface OnItemListener {
        void onClick(View v, int pos, String projectc);
    }

    public void setDefSelect(int position) {
        this.defItem = position;
        notifyDataSetChanged();
    }

    public FamilyBean getFamilyBean(){
        if (defItem != -1){
            return list.get(defItem);
        } else {
            return list.get(0);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        MyViewHolder holder = new MyViewHolder(LayoutInflater.from(
                context).inflate(R.layout.recyclerview_item, parent,
                false));
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.phoneNumber.setText(list.get(position).getName());
        holder.phoneProperty.setText(list.get(position).getNum());
        if (defItem != -1) {
            if (defItem == position) {
                holder.imageView.setImageResource(R.drawable.select);
            } else {
                holder.imageView.setImageResource(R.drawable.unselect);
            }
        } else {
            if (position == 0){
                holder.imageView.setImageResource(R.drawable.select);
            }
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    /**
     * ViewHolder的类，用于缓存控件
     */
    class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView phoneNumber;
        TextView phoneProperty;

        public MyViewHolder(View view) {
            super(view);
            imageView = view.findViewById(R.id.recycler_image);
            phoneNumber = view.findViewById(R.id.recyclerview_phone_number);
            phoneProperty = view.findViewById(R.id.recyclerview_phone_property);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemListener != null) {
                        onItemListener.onClick(v, getLayoutPosition(), list.get(getLayoutPosition()).getName());
                    }
                }
            });
        }


    }

}
